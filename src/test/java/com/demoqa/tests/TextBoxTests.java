package com.demoqa.tests;

import com.demoqa.pages.BasePage;
import com.demoqa.pages.TextBoxPage;
import org.junit.jupiter.api.Test;

public class TextBoxTests extends BasePage {

    TextBoxPage textBoxPage = new TextBoxPage();

    @Test
    void fillFormTest() {
        textBoxPage
                .openPage()
                .setUserName()
                .setUserEmail()
                .setCurrentAddress()
                .setPermanentAddress()
                .clickSubmit();
    }


}
