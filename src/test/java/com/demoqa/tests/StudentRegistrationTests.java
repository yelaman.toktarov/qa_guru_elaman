package com.demoqa.tests;

import com.demoqa.pages.BasePage;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class StudentRegistrationTests extends BasePage {

    @DisplayName("Проверка заполнения всей формы. Это текст отображается по причине того что тест был помечен анотацией от junit @DisplayName")
    @Test
    void succesfullTest() {
        studentRegistrationFormPage
                .openPage()
                .setFirstName(testData.firstName)
                .setLastName(testData.lastName)
                .setEmail(testData.eMailL)
                .setGender(testData.gender)
                .setPhoneNumber(testData.phoneNumber)
                .setDateOfBirth(testData.day, testData.month, testData.year)
                .setSubjects(testData.subject)
                .setHobbies(testData.hobbi)
                .setPicture()
                .setCurrentAddress(testData.currentAddress)
                .selectState(testData.state)
                .selectCity(testData.city)
                .submit()
                .checkResult("Date of Birth", testData.day + " " + testData.month + "," + testData.year)
                .checkResult("Student Name", testData.firstName + " " + testData.lastName)
                .checkResult("Student Email", testData.eMailL)
                .checkResult("Gender", testData.gender)
                .checkResult("Mobile", testData.phoneNumber)
                .checkResult("Date of Birth", testData.day + " " + testData.month + "," + testData.year)
                .checkResult("Subjects", testData.subject)
                .checkResult("Hobbies", testData.hobbi)
                .checkResult("State and City", testData.state + " " + testData.city);
    }

    @DisplayName("Провека негативного теста. Тест должен упасть")
    @Test
    void unsuccesfullTest() {
        studentRegistrationFormPage
                .openPage()
                .setFirstName(testData.firstName)
                .setLastName(testData.lastName)
                .setEmail(testData.eMailL)
                .setGender(testData.gender)
                .setPhoneNumber(testData.phoneNumber)
                .setDateOfBirth(testData.day, testData.month, testData.year)
                .setSubjects(testData.subject)
                .setHobbies(testData.hobbi)
                .setPicture()
                .setCurrentAddress(testData.currentAddress)
                .selectState(testData.state)
                .selectCity(testData.city)
                .submit()
                .checkResult("Date of Birth", testData.day + " " + testData.month + "," + testData.year)
                .checkResult("Student Name", testData.firstName + " " + testData.lastName)
                .checkResult("Student Email", testData.eMailL)
                .checkResult("Gender", testData.gender)
                .checkResult("Mobile", testData.phoneNumber)
                .checkResult("Date of Birth", testData.day + " " + testData.month + "," + testData.year)
                .checkResult("Subjects", testData.subject)
                .checkResult("Hobbies", testData.city);
    }

    @Disabled("Можно написать комментарий и номер задачи!") // этот тест не выполнится так как выключен .
    @Test
    void skipTest() {
        studentRegistrationFormPage
                .openPage()
                .setFirstName(testData.firstName)
                .setLastName(testData.lastName)
                .setEmail(testData.eMailL)
                .setGender(testData.gender)
                .setPhoneNumber(testData.phoneNumber)
                .setDateOfBirth(testData.day, testData.month, testData.year)
                .setSubjects(testData.subject)
                .setHobbies(testData.hobbi)
                .setPicture()
                .setCurrentAddress(testData.currentAddress)
                .selectState(testData.state)
                .selectCity(testData.city)
                .submit()
                .checkResult("Date of Birth", testData.day + " " + testData.month + "," + testData.year)
                .checkResult("Student Name", testData.firstName + " " + testData.lastName)
                .checkResult("Student Email", testData.eMailL)
                .checkResult("Gender", testData.gender)
                .checkResult("Mobile", testData.phoneNumber)
                .checkResult("Date of Birth", testData.day + " " + testData.month + "," + testData.year)
                .checkResult("Subjects", testData.subject)
                .checkResult("Hobbies", testData.city);
    }
}
