package com.demoqa.pages;

import com.codeborne.selenide.SelenideElement;
import com.demoqa.pages.components.*;


import static com.codeborne.selenide.Selenide.*;

public class StudentRegistrationFormPage {

    CalendarComponent calendarComponent = new CalendarComponent();
    ResultsTableComponent resultsTableComponent = new ResultsTableComponent();
    CheckBoxComponent checkBoxComponent = new CheckBoxComponent();
    RadioButtonComponent radioButtonComponent = new RadioButtonComponent();
    UploadFileComponent uploadFileComponent = new UploadFileComponent();

    TestData testData = new TestData();

    //локаторы
    SelenideElement
            firstNameLocator = $("#firstName"),
            lastNameLocator = $("#lastName"),
            eMailLocator = $("#userEmail"),
            genderLocator = $("#genterWrapper"),
            phoneNumberLocator = $("#userNumber"),
            subjectsLocator = $("#subjectsInput"),
            hobbiesLocator = $("#hobbiesWrapper"),
            dateOfBirthLocator = $("#dateOfBirthInput"),
            uploadPictureLocator = $("#uploadPicture"),
            currentAddress = $("#currentAddress"),
            stateInput = $("#react-select-3-input"),
            cityInput = $("#react-select-4-input"),
            submitButton = $("#submit");


    //Открытие страницы
    public StudentRegistrationFormPage openPage() {
        open("automation-practice-form");
        executeJavaScript("$('footer').remove()");
        executeJavaScript("$('#fixedban').remove()");
        return this;
    }

    //Заполнение формы
    public StudentRegistrationFormPage setFirstName(String value) {
        firstNameLocator.setValue(value);
        return this;
    }

    public StudentRegistrationFormPage setLastName(String value) {
        lastNameLocator.setValue(value);
        return this;
    }

    public StudentRegistrationFormPage setEmail(String value) {
        eMailLocator.setValue(value);
        return this;
    }

    public StudentRegistrationFormPage setGender(String value) {
        radioButtonComponent.setRadio(genderLocator, value);
        return this;
    }

    public StudentRegistrationFormPage setPhoneNumber(String value) {
        phoneNumberLocator.setValue(value);
        return this;
    }

    public StudentRegistrationFormPage setDateOfBirth(String day, String month, String year) {
        dateOfBirthLocator.click();
        calendarComponent.setDate(day, month, year);
        return this;
    }

    public StudentRegistrationFormPage setSubjects(String value) {
        subjectsLocator.sendKeys(value);
        subjectsLocator.pressEnter();
        return this;
    }

    public StudentRegistrationFormPage setHobbies(String value) {
        checkBoxComponent.setCheckBox(hobbiesLocator, value);
        return this;
    }

    public StudentRegistrationFormPage setPicture() {
        uploadFileComponent.uploadePicture(uploadPictureLocator, testData.getPhotoPath());
        return this;
    }

    public StudentRegistrationFormPage setCurrentAddress(String value) {
        currentAddress.sendKeys(value);
        return this;
    }

    public StudentRegistrationFormPage selectState(String value) {
        stateInput.setValue(value).pressEnter();
        return this;
    }

    public StudentRegistrationFormPage selectCity(String value) {
        cityInput.setValue(value).pressEnter();
        return this;
    }

    public StudentRegistrationFormPage submit() {
        submitButton.submit();
        return this;
    }

    public StudentRegistrationFormPage checkResult(String key, String value) {
        resultsTableComponent.checkResult(key, value);
        return this;
    }


}
