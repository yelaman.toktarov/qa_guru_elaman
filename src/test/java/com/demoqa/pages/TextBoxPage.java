package com.demoqa.pages;

import static com.codeborne.selenide.Selenide.*;

public class TextBoxPage {
    public String textBoxPage = "/text-box";
    String fullName = "Антон";
    String email = "mail@mail.ru";
    String currentAddres = "Current Address";
    String permanentAddres = "Permament Address";


    public TextBoxPage openPage() {
        open(textBoxPage);
        executeJavaScript("$('footer').remove()");
        executeJavaScript("$('#fixedban').remove()");
        return this;
    }
    //String header = $(".main-header").getText();

    public TextBoxPage getHeader() {
        $(".main-header").getText();
        return this;
    }

    public TextBoxPage setUserName() {
        $("[id=userName]").setValue(fullName);
        return this;
    }

    public TextBoxPage setUserEmail() {
        $("[id=userEmail]").setValue(email);
        return this;
    }

    public TextBoxPage setCurrentAddress() {
        $("[id=currentAddress]").setValue(currentAddres);
        return this;
    }

    public TextBoxPage setPermanentAddress() {
        $("[id=permanentAddress]").setValue(permanentAddres);
        return this;
    }

    public TextBoxPage clickSubmit() {
        $("[id=submit]").click();
        return this;
    }


}
