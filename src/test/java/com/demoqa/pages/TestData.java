package com.demoqa.pages;

import java.util.Locale;

import com.demoqa.utils.RandomGenerator;
import com.github.javafaker.Faker;


public class TestData {

    Faker fakerRU = new Faker(new Locale("ru"));
    Faker fakerEn = new Faker(new Locale("en"));

    private String photoPath = "src/test/resources/files/123.jpeg";//todo указать путь к фото

    public String getPhotoPath() {
        return photoPath;
    }

    //Поля на русском
    public String firstName = fakerRU.address().firstName();
    public String lastName = fakerRU.address().lastName();
    public String eMailL = fakerEn.internet().emailAddress();
    public String phoneNumber = fakerRU.phoneNumber().subscriberNumber(10);

    public String month = RandomGenerator.randomMonths();//не мой кода надо понять что сделал
    public String year = String.valueOf(fakerRU.number().numberBetween(1900, 2000));//не мой кода надо понять что сделал
    public String day = String.valueOf(fakerRU.number().numberBetween(10, 28));//не мой кода надо понять что сделал
    public String subject = "Hindi";
    public String hobbi = RandomGenerator.randomHobby();
    public String gender = RandomGenerator.randomGender();
    public String currentAddress = fakerRU.address().fullAddress();
    public String state = "Rajasthan"; // todo сделать перечесление для выпадающего меню
    public String city = "Jaipur"; //todo сделать перечесление для выпадающего меню

}
