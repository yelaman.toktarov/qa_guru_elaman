package com.demoqa.pages.components;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selectors.byText;

public class CheckBoxComponent {

    public void setCheckBox(SelenideElement selenideElement, String value) {
        $(selenideElement).$(byText(value)).click();
    }
}
