package com.demoqa.pages.components;

import com.codeborne.selenide.SelenideElement;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;

public class UploadFileComponent {

    public void uploadePicture(SelenideElement selenideElement, String value) {
        $(selenideElement).uploadFile(new File(value));
    }

}
