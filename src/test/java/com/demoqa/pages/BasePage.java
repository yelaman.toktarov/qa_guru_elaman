package com.demoqa.pages;

import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

public class BasePage {

    protected TextBoxPage textBoxPage = new TextBoxPage();
    protected StudentRegistrationFormPage studentRegistrationFormPage = new StudentRegistrationFormPage();
    protected TestData testData = new TestData();


    @BeforeAll
    static void beforeAll() {

        System.out.println("BeforAll");
        Configuration.browserSize = "1920x1080";
        Configuration.baseUrl = "https://demoqa.com/";
    }

    @BeforeEach
    void beforeEach() {
        //Выполняется перед методам
        System.out.println("BeforeEach");
    }

    @AfterEach
    void afterEach() {
        //Выполняется после метода
        System.out.println("AfterEach");
    }


    @AfterAll
    static void afterAll() {
        //Выполняется после класса
        System.out.println("AfterAll");

    }

}
